package rtedit.converter;

public interface ConverterListener {
  public void htmlReady(String html);
}
