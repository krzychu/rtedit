package rtedit.converter;

public class PlainConverter extends AbstractConverter {

  @Override
  public String convert(String input) {
    String better = input.replaceAll("\\n", "<br>");
    return better;
  }

  @Override
  public String getName() {
    return "Plain";
  }

}
