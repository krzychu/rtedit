package rtedit.converter;

import java.util.LinkedList;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;


public abstract class AbstractConverter implements DocumentListener{
  
  private LinkedList<ConverterListener> listeners = 
      new LinkedList<ConverterListener>();

  
  public abstract String convert(String input);
  public abstract String getName();

  
  public void addListener(ConverterListener listener){
    listeners.add(listener);
  }
  
  public void removeListener(ConverterListener listener){
    listeners.remove(listener);
  }

  protected void fireHtmlReady(String html){
    for(ConverterListener listener : listeners){
      listener.htmlReady(html);
    }
  }
  
  public void insertUpdate(DocumentEvent e) {
    convert(e.getDocument());
  }
  
  public void removeUpdate(DocumentEvent e) {
    convert(e.getDocument());
  }
  

  public void changedUpdate(DocumentEvent e) {
    //Plain text components do not fire these events
  }
  
  public void convert(Document document) {
    int len = document.getLength();
    String text = null;
    try {
      text = document.getText(0, len);
    } catch (BadLocationException e1) {
      e1.printStackTrace();
    }
    
    String html = convert(text);
    fireHtmlReady(html);
  }
}
