package rtedit.gui;

import rtedit.converter.AbstractConverter;

public interface ToolbarListener {
  public void newClicked();
  public void openClicked();
  public void saveClicked();
  public void saveAsClicked();
  
  public void copyClicked();
  public void cutClicked();
  public void pasteClicked();

  public void converterChanged(AbstractConverter converter);
}
