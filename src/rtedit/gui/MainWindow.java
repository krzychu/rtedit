package rtedit.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import rtedit.converter.AbstractConverter;
import rtedit.converter.BBCodeConverter;
import rtedit.converter.PlainConverter;


public class MainWindow extends JFrame 
  implements DocumentListener, ToolbarListener
{

  private Toolbar toolbar = new Toolbar();
  private Editor editor = new Editor();  
  
  private File documentFile;
  private boolean isDirty = false;
  
  private JFileChooser fileChooser = new JFileChooser();
  
  
  public static void main(String[] args) {
    @SuppressWarnings("unused")
    MainWindow mw = new MainWindow();
  }

  
  public MainWindow(){
    makeGUI();
    
    toolbar.addListener(editor);
    toolbar.addConverter(new BBCodeConverter());
    toolbar.addConverter(new PlainConverter());
    toolbar.addListener(this);
    
    
    editor.getDocument().addDocumentListener(this);
    newClicked();
    
    addWindowListener(new WindowAdapter(){
      public void windowClosing(WindowEvent ev){
        if(confirmDocumentClose()){
          System.exit(0);
        }
      }
    });
  }
  
  
  
  
  
  private void makeGUI() {
    setTitle("RTEdit");
    setLayout(new BorderLayout());
    this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    setPreferredSize(new Dimension(800, 600));
    
    add(toolbar, BorderLayout.PAGE_START);
    add(editor, BorderLayout.CENTER);
    
    pack();
    setVisible(true);
  }


  public void openClicked() {
    if(!confirmDocumentClose()){
      return;
    }
    
    File tmp = findWhatToOpen();
    if(tmp != null){
      documentFile = tmp;
      
      try {
        FileInputStream fis = new FileInputStream(documentFile);
        byte[] buffer = new byte[(int) documentFile.length()];
        fis.read(buffer);
        editor.setText(new String(buffer));
        fis.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
      
      setDirty(false);
    }
  }





  public void saveClicked() {
    if(documentFile == null){
      documentFile = findWhereToSave();
    }
    
    if(documentFile != null){
      
      FileWriter fw;
      try {
        fw = new FileWriter(documentFile);
        BufferedWriter writer = new BufferedWriter(fw);
        writer.write(editor.getText());
        writer.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
     
      setDirty(false);
    }
  }


  public void saveAsClicked() {
    File tmp = findWhereToSave();
    if(tmp != null){
      documentFile = tmp;
      saveClicked();
    }
  }


  public void newClicked() {
    if(!confirmDocumentClose()){
      return;
    }
    
    editor.setText("");
    documentFile = null;
    setDirty(false);
  }
  
  
  private boolean confirmDocumentClose(){
    if(isDirty){
      int answer = ask("This document was modified. Do you want to save?");
      switch(answer){
        case(JOptionPane.CANCEL_OPTION):
          return false;
        case(JOptionPane.YES_OPTION):
          saveClicked();
          return true;
        case(JOptionPane.NO_OPTION):
          return true;
      }
    }
    return true;
  }
  
  
  private File findWhereToSave(){
    int answer = fileChooser.showSaveDialog(this);
    if(answer == JFileChooser.APPROVE_OPTION){
      return fileChooser.getSelectedFile();
    }
    else{
      return null;
    }
  }
  
  
  private File findWhatToOpen() {
    int answer = fileChooser.showOpenDialog(this);
    if(answer == JFileChooser.APPROVE_OPTION){
      return fileChooser.getSelectedFile();
    }
    else{
      return null;
    }
  }
  
  
  private void setDirty(boolean d){
    String dp;
    if(documentFile == null)
      dp = "Untitled";
    else
      dp = documentFile.getAbsolutePath();
    
    isDirty = d;
    if(d){
      setTitle("RTedit - *" + dp);
    }
    else{
      setTitle("RTedit - " + dp);
    }
  }

  private int ask(String question){
    int answer =  JOptionPane.showConfirmDialog(this,
        question, "RTedit",
        JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
    return answer;
  }

  
  @Override
  public void changedUpdate(DocumentEvent arg0) {
    setDirty(true);
  }

  @Override
  public void insertUpdate(DocumentEvent arg0) {
    setDirty(true);
  }
  
  @Override
  public void removeUpdate(DocumentEvent arg0) {}




  @Override
  public void copyClicked() {}
  @Override
  public void cutClicked() {}
  @Override
  public void pasteClicked() {}


  @Override
  public void converterChanged(AbstractConverter converter) {}
}
