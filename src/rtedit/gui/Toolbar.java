package rtedit.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JToolBar;

import rtedit.converter.AbstractConverter;




public class Toolbar extends JToolBar implements ActionListener{

  private JButton newButton;
  private JButton openButton;
  private JButton saveButton;
  private JButton saveAsButton;
  
  private JButton copyButton;
  private JButton cutButton;
  private JButton pasteButton;
 
  
  private ArrayList<AbstractConverter> converters 
    = new ArrayList<AbstractConverter>();
  
  private LinkedList<ToolbarListener> listeners
    = new LinkedList<ToolbarListener>();
  
  private JComboBox converterCombo = new JComboBox();
 
  
  public Toolbar(){
    newButton = makeButton("general/New");
    openButton = makeButton("general/Open");
    saveButton = makeButton("general/Save");
    saveAsButton = makeButton("general/SaveAs");
  
    copyButton = makeButton("general/Copy");
    cutButton = makeButton("general/Cut");
    pasteButton = makeButton("general/Paste");
    
    add(newButton);
    add(openButton);
    add(saveButton);
    add(saveAsButton);
    
    addSeparator();
    
    add(copyButton);
    add(cutButton);
    add(pasteButton);
    
    addSeparator();
    
    add(converterCombo);
    converterCombo.addActionListener(this);
    
    setFloatable(false);
  }
  
  
  
  public void addListener(ToolbarListener listener) {
    listeners.add(listener);
  }



  private JButton makeButton(String iconName){
    URL url = Toolbar.class.getResource(
        "/icons/toolbarButtonGraphics/" + iconName + "24.gif");
    ImageIcon img = new ImageIcon(url, iconName);
    JButton b = new JButton(img);
    b.addActionListener(this);
    return b;
  }
  
  
  public void addConverter(AbstractConverter converter){
    converters.add(converter);
    converterCombo.addItem(converter.getName());
  }



  @Override
  public void actionPerformed(ActionEvent ev) {
    Object src = ev.getSource();

    if(src == converterCombo){
      int idx = converterCombo.getSelectedIndex();
      for(ToolbarListener listener : listeners){
        listener.converterChanged(converters.get(idx));
      }
      return;
    }
    
    
    String methodName = null;
    Field[] fields = this.getClass().getDeclaredFields();
    for(Field f : fields){
      try {
        Object obj = f.get(this);
        if(obj == src){
          methodName = f.getName().replaceAll("Button", "Clicked");
        }
      } catch( Exception e) {
        return;
      }
    }
    
    if(methodName != null){
      for(ToolbarListener listener : listeners){
        try {
          Method method = listener.getClass().getMethod(methodName);
          method.invoke(listener);
        } catch (Exception e) {
          return;
        }
      }
    }
  }
}
