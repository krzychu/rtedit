package rtedit.gui;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.Document;

import rtedit.converter.AbstractConverter;
import rtedit.converter.ConverterListener;

public class Editor extends JPanel 
  implements ConverterListener, ToolbarListener
{
  
  private JTextArea editor = new JTextArea();
  private JEditorPane rendered = new JEditorPane();
  
  private AbstractConverter converter = null;
  
  public Editor(){
    makeGUI();
  }
  
  
  public void setText(String text){
    editor.setText(text);
  }

  
  public void setConverter(AbstractConverter c){ 
    if(converter != null){
      editor.getDocument().removeDocumentListener(converter);
      converter.removeListener(this);
    }
    
    converter = c;
    
    if(converter != null){
      editor.getDocument().addDocumentListener(converter);
      converter.addListener(this);
      converter.convert(editor.getDocument());
    }
  }
  
  
  
  private void makeGUI() {
    setLayout(new GridLayout(1,2));
    
    Font font = new Font("Monospaced", Font.PLAIN, 16);
    editor.setFont(font);
    editor.setLineWrap(true);
    
    
    JScrollPane editScroll = new JScrollPane(editor);
    add(editScroll);
    
    
    rendered.setFont(font);
    rendered.setContentType("text/html");
    rendered.setEditable(false);
    //rendered.setBackground(Color.BLACK);
    //rendered.setForeground(Color.WHITE);
    
    JScrollPane renderedScroll = new JScrollPane(rendered);
    //renderedScroll.set
    add(renderedScroll);
  }


  @Override
  public void htmlReady(String html) {
    rendered.setText(html);
  }


  public Document getDocument() {
    return editor.getDocument();
  }


  public String getText() {
    return editor.getText();
  }


  @Override
  public void newClicked() {}
  @Override
  public void openClicked() {}
  @Override
  public void saveClicked() {}
  @Override
  public void saveAsClicked() {}


  @Override
  public void copyClicked() {
    editor.copy();
  }


  @Override
  public void cutClicked() {
    editor.cut();
  }


  @Override
  public void pasteClicked() {
    editor.paste();
  }


  @Override
  public void converterChanged(AbstractConverter converter) {
    setConverter(converter);
  }
  
}
